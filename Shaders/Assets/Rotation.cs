﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public float speed = 15f;

    // Update is called once per frame
    public Vector3 position1;
    public Vector3 position2;

    void Update()
    {


        transform.localEulerAngles = new Vector3( Mathf.PingPong(Time.time * speed, 60), -30, transform.position.z);


    }
}
