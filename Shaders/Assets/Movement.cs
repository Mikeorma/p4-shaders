﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    public float speed = 2.5f;

    // Update is called once per frame
    public Vector3 position1;
    public Vector3 position2;

    void Update()
    {


        transform.position = new Vector3(transform.position.x, Mathf.PingPong(Time.time*speed, 2), transform.position.z);


    }
}

